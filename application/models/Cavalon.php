<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cavalon extends CI_Model {

	public function getLoginCredentials($username){
		$query = 'SELECT * FROM user WHERE username = ?';
		$loginCredentials = $this->db->query($query, array('username' => $username))->row_array();
		return $loginCredentials;
	}

	public function updatePassword($passwordInfo){
		$query = "UPDATE user SET password = ?, salt = ? WHERE id = ?";
		$updated = $this->db->query($query, array('password' => $passwordInfo['password'], 'salt' => $passwordInfo['salt'], 'id' => $_SESSION['userID']));
		return $updated;
	}

	public function updateTenant($tenantInfo){
		$query = "UPDATE user SET first_name = ?, last_name = ?, email = ?, updated_at = NOW() WHERE id = ?";
		$updated = $this->db->query($query, array('first_name' => $tenantInfo['first_name'], 'last_name' => $tenantInfo['last_name'], 'email' => $tenantInfo['email'], 'id' => $_SESSION['userID']));
		return $updated;
	}

	public function getTenantInfo($userID){
		$query = "SELECT * FROM user WHERE id = ?";
		$tenantInfo = $this->db->query($query, array('id' => $userID))->row_array();
		return $tenantInfo;
	}

	public function getAllMessages($ownerID){
		$query = 'SELECT messages.title, messages.content, messages.created_at, messages.viewed, user.first_name, user.last_name, user.company FROM messages LEFT JOIN user ON messages.authorID = user.id WHERE ownerID = ?';
		$allMessages = $this->db->query($query, array('ownerID' => $ownerID))->result_array();
		return $allMessages;
	}

	public function getNewMessageCount($userID){
		$query = 'SELECT id FROM messages WHERE ownerID = ? AND viewed = ?';
		$allNewMessages = $this->db->query($query, array('ownerID' => $_SESSION['userID'], 'viewed' => 'no'))->result_array();
		return $allNewMessages; 
	}
}
