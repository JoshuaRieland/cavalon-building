<html>
<head>
	<title></title>

    <!-- CSS -->
	<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/assets/css/main.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/sky-form.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- FONTS -->
    <link href='https://fonts.googleapis.com/css?family=Lobster|Abril+Fatface' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Cinzel:400,700' rel='stylesheet' type='text/css'>

    <!-- JAVASCRIPT -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    
    <script type="text/javascript">
    	// jQuery for page scrolling feature - requires jQuery Easing plugin
	    $(function() {
	        $('a.page-scroll').bind('click', function(event) {
	            var $anchor = $(this);
	            $('html, body').stop().animate({
	                scrollTop: $($anchor.attr('href')).offset().top
	            }, 2000, 'easeInOutExpo');
	            event.preventDefault();
	        });
	    });

	    // AJAX Loading Spinner
        $(document).ready(function(){
            var $loading = $('.loadingDiv').hide();
        
            $(document)
                .ajaxStart(function () {
                    $loading.show();
                })
                .ajaxStop(function () {
                    $loading.hide();
                });
        })
    </script>

</head>
<body>

	<?php $this->load->view('navbar'); ?>

    <section id='contact-section'>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 margin30 sky-form-columns">
                    <h3 class="heading">Chat With Us</h3>
                    <p>
                        Are you interested in moving your business to Cavalon Place? Let us know what information you might be needing. We're happy to supply you with building ammenity lists, budget reports, floor plan customization and regualtions or any other inquiry you might have.
                    </p>
                    <div class='message'></div>
                    <form class="sky-form" action="/contact-message" method="post" id="sky-form">
                        <div class='loadingDiv' style='width: 50px; height: 50px; margin: 0 auto;  z-index: 9999;'>
                            <i style='font-size: 50px; color: lightblue; display: inline-block;' class="fa fa-cog fa-spin"><i style='font-size: 50px; color: lightblue; display: inline-block;' class="fa fa-cog fa-spin"></i></i>
                        </div>
                        <fieldset>                  
                            <div class="row">
                                <section class="col col-6">
                                    <label class="label">Name</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="text" name="name" id="name" required>
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="label">E-mail</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-envelope-o"></i>
                                        <input type="email" name="email" id="email" required>
                                    </label>
                                </section>
                            </div>
                            
                            <section>
                                <label class="label">Company</label>
                                <label class="input">
                                    <i class="icon-append fa fa-briefcase"></i>
                                    <input type="text" name="company" id="company" required>
                                </label>
                            </section>

                             <section>
                                <label class="label">Subject</label>
                                <label class="input">
                                    <i class="icon-append fa fa-comment"></i>
                                    <input type="text" name="subject" id="subject" required>
                                </label>
                            </section>
                            
                            <section>
                                <label class="label">Message</label>
                                <label class="textarea">
                                    <i class="icon-append fa fa-commenting-o"></i>
                                    <textarea rows="4" name="message" id="message" required></textarea>
                                </label>
                            </section>
                            
                            <section>
                                <label class="label">Enter characters below:</label>
                                <?php echo $image; ?><br><br>
                                <label class="input input-captcha">
                                    <input type="text" maxlength="8" name="captcha" id="captcha" placeholder="Prove you're human" required>
                                </label>
                            </section>
                            
                            <section>
                                <label class="checkbox"><input type="checkbox" name="copy" value='on'><i></i>Send a copy to my e-mail address</label>
                            </section>
                        </fieldset>
                        <div class='loadingDiv' style='width: 50px; height: 50px; margin: 0 auto;  z-index: 9999;'>
                            <i style='font-size: 50px; color: lightblue; display: inline-block;' class="fa fa-cog fa-spin"><i style='font-size: 50px; color: lightblue; display: inline-block;' class="fa fa-cog fa-spin"></i></i>
                        </div>
                        
                        <button type="submit" class="btn border-white btn-lg">Let's Chat</button>
                        
                        
                        <div class="message">
                            <i class="fa fa-check"></i>
                            <p>Your message was successfully sent!</p>
                        </div>
                    </form> 
                   
                </div>
                <!-- <div class="col-md-4">
                    <h3 class="heading">Contact info</h3>
                    <ul class="list-unstyled contact contact-info">
                        <li>
                            <p><strong><i class="fa fa-map-marker"></i> Address:</strong> 220 2nd Ave S</p>
                            <p id='address_alignment'>Seattle, WA 98105</p>
                        </li> 
                        <li><p><strong><i class="fa fa-envelope"></i> Mail Us:</strong> <a href="mailto:info@uxacademy.io">info@uxacademy.io</a></p></li>
                    </ul>
                    <div class="divide40"></div>
                    -->
            </div>
        </div><!--contact advanced container end-->
    </section>

    <!-- Embedded Google Maps -->
   	<section class='google_maps'>
    	<div id="map_canvas"></div>
   	</section>
    

    <!-- Load Footer -->
    <?php $this->load->view('footer'); ?>


     <!-- jQuery -->
    <script src="/assets/js/jquery.js"></script>

    <!-- Plugin JavaScript -->
    <script src="/assets/js/jquery.easing.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/assets/js/bootstrap.min.js"></script>

    <!--gmap js-->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript">
        var myLatlng;
        var map;
        var marker;

        function initialize() {
            myLatlng = new google.maps.LatLng(47.652278, -122.677719);

            var mapOptions = {
                zoom: 13,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: false,
                draggable: true
            };
            map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

            var contentString = '<p style="line-height: 20px;"><strong>assan Template</strong></p><p>Vailshali, assan City, 302012</p>';

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: 'Marker'
            });

            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map, marker);
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>


</body>
</html>