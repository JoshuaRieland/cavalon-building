<html>
<head>
	<title>Tenant Dashboard</title>
	<style type="text/css">
		#welcomeMessage{
			margin-bottom: 25px;
		}
		.content{
			border: 2px solid white;
			border-radius: 10px;
			background-color: black;
			color: white;
			height: 200px;
			padding: 10px;
			opacity: 0.9;
		}
			.contentHeader{
				text-align: center;
				text-decoration: underline;
			}
	</style>
	</style>
</head>
<body>
	<?php if($session['status'] = 'Active'){
	 $this->load->view('tenant_navbar'); ?>
	 	<div class='container'>
	 		<div class='row' id='welcomeMessage'>
	 			<h3>Welcome <?=$_SESSION['tenantInfo']['first_name']?>&nbsp<?=$_SESSION['tenantInfo']['last_name']?>!</h3>
	 			<br>
	 			<h4>You have <?=$_SESSION['newMessagesCount']?> new <a href="/show/messages">messages</a>.</h4>
	 		</div>
	 		<div class='row'>
	 			<div class='col-xs-12 col-md-6 content'>
	 				<h3 class='contentHeader'>Urgent Notification Board</h3>
	 				<ul>
	 					<li>New Parking Policy</li>
	 					<li>Elevator 1 Down For Rest of Week</li>
	 					<li>Christmas Decor Policy</li>
	 				</ul>
	 			</div>
	 			<div class='col-xs-12 col-md-6 content'>
	 				<h3 class='contentHeader'>Upcoming Events</h3>
	 			</div>
	 		</div>
	 	</div>
	<?php  } ?>
</body>
</html>


<br><br><br><br><br><br><br><br><br><br>
 <?php
 var_dump($_SESSION);
?>