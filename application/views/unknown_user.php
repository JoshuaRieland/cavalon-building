<html>
<head>
	<title>Unknown User</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<style type="text/css">
		*{
			text-align: center;
			margin-top: 50px;
		}
	</style>
</head>
<body>
	<div class='col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1 col-md-4 col-md-offset-4'>
		<img src="/assets/images/oops.gif">
		<h4>OOPS! Looks like that username or password is incorrect!</h4>
		<h4>If you are a current tenant, please check your log in credentials and try again.</h4>
		<h4>Contact your portal administor/manager if issues persist.</h4>
		<a href="/home"><h4>Return To Home Page</h4></a>
	</div>
</body>
</html>