<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta charset="utf-8">
	
	<title>Available Space</title>

	<!-- CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/sky-form.css">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/main.css">

	<!-- FONTS -->
	<link href='https://fonts.googleapis.com/css?family=Lobster|Abril+Fatface' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Cinzel:400,700' rel='stylesheet' type='text/css'>

	<!-- JAVASCRIPTS -->
	<script src="/assets/js/JQueryLib.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	
	<script type="text/javascript">

		// AJAX Info Inquire Form
	    $(document).on('submit', 'form#inquiry_form', function(){
	        $.post(
	            $(this).attr('action'),
	            $(this).serialize(),
	            function(returned_data){
	                console.log(returned_data);
	                $('form#inquiry_form').hide();
	                setTimeout(function() {$('form#inquiry_form').show(); }, 5000);
	                $('div.message').append(
	                    "<h5 id='form_return_message'>"+returned_data+"</h5>"
	                )
	                setTimeout(function() {$('h5#form_return_message').remove(); }, 5000);
	            },
	            "json"
	        )   
	        return false; 
	    });

	   
	    // AJAX Loading Spinner
	    $(document).ready(function(){
	    var $loading = $('.loadingDiv').hide();
	    $(document)
	  		.ajaxStart(function () {
	        	$loading.show();
	      	})
	      	.ajaxStop(function () {
	        	$loading.hide();
	      	});
	    })



	</script>
</head>
<body>

	<!-- Load Navigation -->
	<?php $this->load->view('navbar.php'); ?>
	
	<section id='available_space_headline'>
		<div class='container'>
			<h2>Customizable Space With Amazing Views</h2>
		</div> <!-- END Container -->
	</section>
	<section id='available_space_plans'>
		<div class='container'>

		</div>
	</section>
	<section id='available_space_inquiry'>
		<div class='container'>
			<h2>More Info?</h2>
			<h3>We'd be delighted to connect with you and answer</h3>
			<h3>any questions you may have or schedule a viewing.</h3>
			<!-- <form action='inquire/available_space' id='inquiry_form' method='POST' action='inquire/available_space'>
				<div class='form_lineup'>
					<input type='text' name='name' placeholder='Your Name' id='inquiry_name' required title='Please enter your name.'>
					<input type='text' name='company' placeholder='Company' id='inquiry_company' required>
					<input type='email' name='email' placeholder='Email Address' id='inquiry_email' required>
				</div>
				<div class='form_lineup'>
					<textarea name='content' placeholder='Describe how we can help you.' required></textarea>
				</div>
				<div class='row'>
					<button type='submit'>Talk To Us</button>
				</div>
			</form> -->
		<div class='container'>
			<div class='col-md-8 col-md-offset-2'>
				<form class="sky-form" action="" method="post" id="inquiry-form">
                    <div class='loadingDiv' style='width: 50px; height: 50px; margin: 0 auto;  z-index: 9999;'>
                        <i style='font-size: 50px; color: lightblue; display: inline-block;' class="fa fa-cog fa-spin"><i style='font-size: 50px; color: lightblue; display: inline-block;' class="fa fa-cog fa-spin"></i></i>
                    </div>
                        <fieldset>                  
                            <div class="row">
                                <section class="col col-6">
                                    <label class="label">Name</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="text" name="name" id="name" required>
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="label">E-mail</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-envelope-o"></i>
                                        <input type="email" name="email" id="email" required>
                                    </label>
                                </section>
                            </div>
                            
                             <section>
                                <label class="label">Subject</label>
                                <label class="input">
                                    <i class="icon-append fa fa-comment"></i>
                                    <input type="text" name="subject" id="subject" required>
                                </label>
                            </section>
                            
                            <section>
                                <label class="label">Message</label>
                                <label class="textarea">
                                    <i class="icon-append fa fa-commenting-o"></i>
                                    <textarea rows="4" name="message" id="message" required></textarea>
                                </label>
                            </section>
                           
                            
                            <section>
                                <label class="label">Enter characters below:</label>
                                
                                <?php echo $image; ?>
                                <label class="input input-captcha" id='captcha'>
                                	<img src="/assets/icons/robot30.svg" class='icon-append' style='width:33px; height:29px;'>
                                	<!-- <i class='icon-append fa fa-android'></i> -->
                                    <input type="text" maxlength="8" name="captcha" id="captcha" placeholder="Prove you're human" required>
                                </label>
                            </section>
                            
                        </fieldset>
                        <div class='loadingDiv' style='width: 50px; height: 50px; margin: 0 auto;  z-index: 9999;'>
                            <i style='font-size: 50px; color: lightblue; display: inline-block;' class="fa fa-cog fa-spin"><i style='font-size: 50px; color: lightblue; display: inline-block;' class="fa fa-cog fa-spin"></i></i>
                        </div>
                        
                        <button type="submit" class="btn border-white btn-lg">Let's Chat</button>
                        
                        
                        <div class="message">
                            <i class="fa fa-check"></i>
                            <p>Your message was successfully sent!</p>
                        </div>
                    </form> 
            </div>
        </div>
			<div class='message'></div>
		</div>


	</section>
	<section class='google_maps'>
	    <div id="map_canvas"></div>
	</section>   
	
	

	
	

	<!-- Load Footer -->
	<?php $this->load->view('footer'); ?>




	<!--gmap js-->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript">
        var myLatlng;
        var map;
        var marker;

        function initialize() {
            myLatlng = new google.maps.LatLng(47.652278, -122.677719);

            var mapOptions = {
                zoom: 13,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: false,
                draggable: true
            };
            map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

            var contentString = '<p style="line-height: 20px;"><strong>assan Template</strong></p><p>Vailshali, assan City, 302012</p>';

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: 'Marker'
            });

            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map, marker);
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>

</body>
</html>