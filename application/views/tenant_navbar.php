<html>
<head>
	<title>Tenant Navbar</title>
</head>
<body>
<html>
<head>
	<title>Navbar</title>
	<meta charset="utf-8">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	</style>
</head>
<body>

	<!-- START NAVBAR -->
		<nav class="navbar navbar-default navbar-responsive centerText" id='navbar'>
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="/home">Cavalon Img</a>
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li><a href="/tenant_dashboard">Dashboard</a></li>
		        <li><a href="/show/messages">Messages</a></li>
		        <li><a href="#">Community News</a></li>
		        <li><a href="#">Budget Info</a></li>
		        <li><a href="#">Photos</a></li>
		        <li class="dropdown">
		        	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Property<br>Management<span class="caret"></span></a>
		        	<ul class='dropdown-menu'>
		        		<li><a href="#">Building Rules</a></li>
		        		<li role="separator" class="divider"></li>
		        		<li><a href="#">Parking</a></li>
		        	</ul>
		        </li>

		        <li class='dropdown'>
		        	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Maintenance<span class="caret"></span></a>
		        	<ul class="dropdown-menu">
		        		<li><a href="#">Submit Request</a></li>
		        		<li role="separator" class="divider"></li>
		        		<li><a href="#">Current Requests</a></li>
		        	</ul>
		        </li>

		      </ul>
		      <ul class="nav navbar-nav navbar-right">
		        <li class='dropdown'>
		        	<a href="#" class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>My Account<span class="caret"></a>
		        	<ul class='dropdown-menu'>
		        		<li><a href="/account/settings"><span class='glyphicon glyphicon-cog' aria-hidden='true'></span>&nbsp&nbspSettings</a></li>
		        		<li><a href="/account/signout"><span class='glyphicon glyphicon-log-out' aria-expanded='true'></span>&nbsp&nbspSign Out</a></li>
		        	</ul>
		        </li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
<!-- END NAVBAR -->


</body>
</html>
</body>
</html>