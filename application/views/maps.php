<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta charset="utf-8">

	<title>Maps</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

	<script src="/assets/js/JQueryLib.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	
	<style type="text/css">
		.centerText{
			text-align: center;
		}
	</style>
</head>
<body>
	<?php $this->load->view('navbar'); ?>
	<div id='container'>
		<!-- Main Body Content -->
			<h2 class='centerText'>Maps Displayed Here</h2>
		<!-- END: Main Body Content -->
	</div> <!-- END Container -->
</body>
</html>