<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<title>Cavalon Place</title>
	
	<!-- CSS Stylesheets -->
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/main.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/animate.css">

	<!-- Font Stylesheets -->
	<link href='https://fonts.googleapis.com/css?family=Lobster|Abril+Fatface' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Cinzel:400,700' rel='stylesheet' type='text/css'>
	<script src="/assets/js/JQueryLib.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<!-- // <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script> -->
</head>
<body>

	<!-- START CONTAINER -->
	<?php $this->load->view('navbar.php'); ?>
	<section id='attention_grab'>
					<div class='row'>
					<figure id='sign'>
						<img src="/assets/images/cavalon_sign_slim.svg" id='cavalon_sign'>
					</figure>
					</div>

		<div class='row'>
			<div class='col-xs-12' id='cavalon_welcome'>
				<div class='col-xs-12 col-sm-6 col-md-offset-1' id='welcome_message'>
					
					<p>
						Welcome to Cavalon Building. Located on Ridgetop Blvd overlooking Silverdale and Dyes Inlet.
					</p>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</p>
				</div>
				<div class='col-xs-12 col-sm-6 col-md-4 col-sm-offset-0' id='cavalon_building_image'>
					<img src="/assets/images/cavalon_front.jpg">
				</div>
			</div>
		</div>
	</section>

	<!-- Start: Current Tenants -->
	<section id='cavalon_tenants'>
		<!-- <div class='container'> -->
			<div class='row'>
				<h2 class='centerText'>Proudly Serving Our Friendly Tenants</h2>
				<div class='col-sm-11 col-xs-offset-0 col-sm-offset-1'>			
					<a href="http://www.kitsapoms.com/" target='_'>
						<figure class='tenant_logos'>
							<img src="/assets/images/oralsurgeryclinic.png" alt='Oral Surgery & Implant Clinic' id='oral_surgery'>
							<figcaption>Oral Surgery & Implant Clinic</figcaption>
						</figure>
					</a>
					<a href="http://www.scottstewartdmd.com/" target='_'>
						<figure class='tenant_logos'>
							<img src="/assets/images/scottstewart.svg" alt='Scott W Stewart DMD PLLC' id='scottstewart'>
							<figcaption>Scott W. Stewart DMD PLLC</figcaption>
						</figure>
					</a>
					<a href="https://www.paclab.com/" target='_'>
						<figure class='tenant_logos'>
							<img src="/assets/images/paclab.svg" alt='PACLAB' id='paclab'>
							<figcaption>PACLAB Network Laboratories</figcaption>
						</figure>
					</a>
					<a href="http://www.pnwtkitsap.com/" target='_'>
						<figure class='tenant_logos'>
							<img src="/assets/images/pnwt.svg" alt='Pacific Northwest Title of Kitsap County' id='pnwt'>
							<figcaption>Pacific Northwest Title</figcaption>
						</figure>
					</a>
					<a href="https://www.evergreenhomeloans.com/" target='_'>
						<figure class='tenant_logos'>
							<img src="/assets/images/evergreen_homeloans.svg" alt='Evergreen Homeloans' id='evergreen_homeloans'>
							<figcaption>Evergreen Home Loans</figcaption>
						</figure>
					</a>
					<a href="http://renalremission.com/" target='_'>
						<figure class='tenant_logos'>
							<img src="/assets/images/renalremission.svg" alt='Renal Remission' id='renalremission'>
							<figcaption>Renal Remission</figcaption>
						</figure>
					</a>
					<a href="http://www.dalewalters.com/" target='_'>
						<figure class='tenant_logos'>
							<img src="/assets/images/dalewaters.svg" alt='Dale Walters Real Estate' id='dalewalters'>
							<figcaption>Dale Walters Real Estate</figcaption>
						</figure>
					</a>
					<a href="http://www.fgs-llc.com/" target='_'>
						<figure class='tenant_logos'>
							<img src="/assets/images/FGS.svg" alt='FGS LLC' id='FGS'>
							<figcaption>FGS LLC</figcaption>
						</figure>
					</a>
					<a href="http://kylekincaid.com/" target='_'>
						<figure class='tenant_logos'>
							<img src="/assets/images/kyle_kincaid.svg" alt='Kyle Kincaid' id='kyle_kincaid'>
							<figcaption>Kyle Kincaid CPA</figcaption>
						</figure>
					</a>
					<!-- <a href="" target='_'> -->
						<figure class='tenant_logos'>
							<img src="/assets/images/big_sky.svg" alt='Big Sky Custom Home Builders' id='big_sky'>
							<figcaption>Big Sky Builders</figcaption>
						</figure>
					<!-- </a> -->
				</div>
			</div>
		<!-- </div> -->
	</section>
	<!-- End: Current Tenants -->
	<section id='available_space_cta'>
		<div class='container'>
			<div class='col-xs-12' id='availavble_space_message'>
				<h2>Seeking a home for your business?</h2>
				<h2>We have available space ready for your customization.</h2>
				<a href="show/available_space">
					<h2 id='join_us'>
						<i class='glyphicon glyphicon-hand-right'></i>
						Join Us At Cavalon
						<i class='glyphicon glyphicon-hand-left'></i>
					</h2>
				</a>
			</div>
		</div>
		
	</section>
	
	<!-- Start: Google Maps -->
	<section id='google_maps'>
	    <div id="map_canvas"></div>
	</section>
	<!-- End: Google Maps -->

	<!-- Load Footer -->
	<?php $this->load->view('footer'); ?>
	

    <!--gmap js-->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript">
        var myLatlng;
        var map;
        var marker;

        function initialize() {
            myLatlng = new google.maps.LatLng(47.652278, -122.677719);

            var mapOptions = {
                zoom: 13,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: false,
                draggable: true
            };
            map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

            var contentString = '<p style="line-height: 20px;"><strong>assan Template</strong></p><p>Vailshali, assan City, 302012</p>';

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: 'Marker'
            });

            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map, marker);
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</body>
</html>