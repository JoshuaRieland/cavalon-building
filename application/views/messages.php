<html>
<head>
	<title>Tenant Dashboard</title>
	<style type="text/css">
		table{
			width: 100%;
			padding: 10px;
		}
			.mailIcon{
				text-align: center;
				padding: 5px;
			}
			.mailCells{
				padding: 5px;
				padding-left: 15px;
			}
		#messageTable{
			border: 10px groove silver;
			/*border-radius: 5px;*/
			display: inline-block;
			height: 500px;
			overflow-y: scroll;
		}
		#viewingPane{
			height: 500px;
			margin: 0 auto;
			border: 10px groove silver;
			overflow-y: scroll; 
		}

	</style>
</head>
<body>
	<?php if($session['status'] = 'Active'){
	 $this->load->view('tenant_navbar'); ?>
	 	<div class='container'>
	 		<div class='col-xs-12 col-md-6' id='messageTable'>
		 		<?php if(count($allMessages) == 0){ ?>
		 			<p>You have no messages.</p>
		 		<?php } else { ?>
		 			<table class='table-responsive table-striped'>
		 				<thead>
		 					<tr>
		 						<th class='mailCells'></th>
		 						<th class='mailCells'>Date</th>
		 						<th class='mailCells'>From</th>
		 						<th class='mailCells'>Message</th>
		 					</tr>
		 				</thead>
		 				<tbody>
		 					<?php foreach($allMessages as $message){ 
		 						$date = strtotime($message['created_at']); ?>
		 						<tr>
		 							<td class='mailIcon'>
		 								<?php if($message['viewed'] == 'no'){?>
		 									<span class='glyphicon glyphicon-envelope' style='color:blue'></span>
		 								<?php } else { ?>
		 									<span class='glyphicon glyphicon-envelope'></span>
		 								<?php } ?>
		 							</td>
		 							<td class='mailCells'><?= date('M d,y', $date) ?> <?= date('h:ma', $date) ?></td>
		 							<td class='mailCells'><?=$message['last_name']?>,&nbsp<?=$message['first_name']?></td>
		 							<td class='mailCells'><?=$message['title']?></td>
		 						</tr>
		 					<?php }	?>
		 				</tbody>
		 			</table>	
		 		<?php } ?>
	 		</div><!-- END: Message Table -->
	 		<div class='col-xs-12 col-md-6' id='viewingPane'>
	 			<div class='row' id='readingPane'>

	 			</div>
	 			<div class='row' id='messageForm'>

	 			</div>
	 		</div>
	 	</div>
	<?php  } ?>
</body>
</html>


<br><br><br><br><br><br><br><br><br><br>
 <?php
 var_dump($_SESSION);
?>