<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta charset="utf-8">

	<title>Register User</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

	<script src="/assets/js/JQueryLib.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	
	<style type="text/css">
		#container{
			margin-top: 100px;
			border: 5px solid black;
		}
			.centerText{
				text-align: center;
			}
			form{
				margin-top: 50px;
			}
	</style>
	<script type="text/javascript">
	$(document).ready(function(){
			var password = document.getElementById("new_password");
			var confirm_password = document.getElementById("confirm_password");
			function validatePassword(){
			  if(password.value != confirm_password.value) {
			    confirm_password.setCustomValidity("Passwords Don't Match");
			  } else {
			    confirm_password.setCustomValidity('');
			  }
			}
			password.onchange = validatePassword;
			confirm_password.onkeyup = validatePassword;
	})
	</script>
</head>
<body>
	
	<div class='col-xs-6 col-xs-offset-3' id='container'>
		<!-- Main Body Content -->
			<div class='centerText'>
				<h4>Password Registration</h4>
				<p>Please change your password before continuing on.</p>
				<p>Password must be 7 character long.</p>
				<p>For increased security use both upper and lower case letters in combination with numbers/symbols.</p>
			</div>

			<form class="form-horizontal pure-form" action='/update_password' method='POST'>
			  	
			  	<div class="form-group">
			    	<label for="new_password" class="col-sm-6 control-label">New Password</label>
			    	<div class="col-sm-6">
			     		<input type="password" class="form-control" name='new_password' id="new_password" placeholder="New Password" pattern=".{7,}" required title='Minimum Length: 7 Characters'>
			    	</div>
			  	</div>
			  	<div class="form-group">
			    	<label for="confirm_password" class="col-sm-6 control-label">Password Confirmation</label>
			    	<div class="col-sm-6">
			      		<input type="password" class="form-control" name='confirm_password' id="confirm_password" placeholder="Confirm Password" pattern=".{7,}">
			    	</div>
			  	</div>
			  
			  	<div class="form-group">
			    	<div class="col-sm-offset-6 col-sm-2">
			      		<button type="submit" class="btn btn-default">Submit</button>
			    	</div>
			  	</div>
			</form>
		<!-- END: Main Body Content -->
	</div> <!-- END Container -->
</body>
</html>