<html>
<head>
	<title>Navbar</title>
	<meta charset="utf-8">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="/assets/css/main.css">
</head>
<body id='navigation'>

	<!-- START NAVBAR -->
		<nav class="navbar navbar-default navbar-responsive centerText" id='navbar'>
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <?php if((isset($_SESSION['status']) && $_SESSION['status'] != 'Active') || !isset($_SESSION['status'])){ ?>
		      <a class="navbar-brand" href="/home">Cavalon<br>Place<p class='brand_subtitle'>At the Highlands</p></a>
		      <?php }else{ ?>
		      <span class='navbar-brand' href=''>Cavalon<br>Place<p class='brand_subtitle'>At the Highlands</p></span>
		      <?php } ?>

		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        
		        <li><a href="/show/available_space">Available<br>Space</a></li>
		        <li><a href="/show/floor_plans">Floor Plans</a></li>
		        <!-- <li><a href="/show/maps">Maps</a></li> -->
		        <!-- <li><a href="#">Budget Info</a></li> -->
		        <li><a href="/show/photos">Photos</a></li>
		        <li class="dropdown">
		        	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Property<br>Management<span class="caret"></span></a>
		        	<ul class='dropdown-menu'>
		        		<li class='nav_highlight'><a href="#">Building Rules</a></li>
		        		<li role="separator" class="divider"></li>
		        		<li class='nav_highlight'><a href="#">Parking</a></li>
		        	</ul>
		        </li>

		        <li class='dropdown'>
		        	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Vendors<span class="caret"></span></a>
		        	<ul class="dropdown-menu">
		        		<li><a href="#">Approved Vendors</a></li>
		        		<li role="separator" class="divider"></li>
		        		<li><a href="#">New Vendor <br> Application</a></li>
		        	</ul>
		        </li>

		      </ul>

		      <ul class="nav navbar-nav navbar-right">
		      		<li><a href="/contact-us">Contact Us</a></li>
		      	<?php if((isset($_SESSION['status']) && $_SESSION['status'] != 'Active') || !isset($_SESSION['status'])){ ?>
			        <li class='dropdown'>
			        	<a href="#" class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>Tentant<br>SignIn</a>
			        	<ul class='dropdown-menu'>
			        		<form method='post' action='/account/signin' id='signInForm'>
			        			
			        			<label>Username:</label>
			        			<input type='text' required='true' name='username' id='username'>
			        			
			        			<label>Password</label>
			        			<input type='password' required='true' name='password' id='userpassword'>
			        			<br><br>
			        			<input type='submit' value='Sign In' id='signInBtn'>
			        		</form>
			        	</ul>
			        </li>
		     	<?php }else{ ?>
		     		<li><a href="/tenant_dashboard">Go To Dashboard</a></li>
		      	<?php } ?>
		      </ul>
		      	
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
<!-- END NAVBAR -->


</body>
</html>