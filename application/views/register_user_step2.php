<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta charset="utf-8">

	<title>Register User</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

	<script src="/assets/js/JQueryLib.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	
	<style type="text/css">
		#container{
			margin-top: 100px;
			border: 5px solid black;
		}
			.centerText{
				text-align: center;
			}
			form{
				margin-top: 50px;
			}
	</style>
	
</head>
<body>
	
	<div class='col-xs-6 col-xs-offset-3' id='container'>
		<!-- Main Body Content -->
			<div class='centerText'>
				<h4>Tenant Registration</h4>
				<p>Please complete the following form to finish the registration process.</p>
			</div>

			<form class="form-horizontal pure-form" action='update_tenant' method='POST'>
			  	<input type='hidden' name='userID' value='<?= $_SESSION['userID'] ?>'>
			  	<div class="form-group">
			    	<label for="first_name" class="col-sm-6 control-label">First Name</label>
			    	<div class="col-sm-6">
			     		<input type="text" class="form-control" name='first_name' id="first_name" placeholder="First Name" pattern=".{2,}" required title='Enter A Valid Name'>
			    	</div>
			  	</div>
			  	<div class="form-group">
			    	<label for="last_name" class="col-sm-6 control-label">Last Name</label>
			    	<div class="col-sm-6">
			     		<input type="text" class="form-control" name='last_name' id="last_name" placeholder="First Name" pattern=".{2,}" required title='Enter A Valid Name'>
			    	</div>
			  	</div>
			  	<div class="form-group">
			    	<label for="email" class="col-sm-6 control-label">Email</label>
			    	<div class="col-sm-6">
			     		<input type="email" class="form-control" name='email' id="email" placeholder="Email Address" required>
			    	</div>
			  	</div>
			  
			  	<div class="form-group">
			    	<div class="col-sm-offset-6 col-sm-2">
			      		<button type="submit" class="btn btn-default">Submit</button>
			    	</div>
			  	</div>
			</form>
		<!-- END: Main Body Content -->
	</div> <!-- END Container -->
</body>
</html>