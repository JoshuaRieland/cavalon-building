<!DOCTYPE html>
<html>
<head>
	<title>Cavalon Photos</title>

	<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">
	<!-- <link rel="stylesheet" type="text/css" href="/assets/css/student_dashboard.css"> -->
	<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> -->
  	<link rel="stylesheet" href="/resources/demos/style.css">
  	<link rel="stylesheet" type="text/css" href="/assets/css/sky-form.css">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<script type="text/javascript" src='/assets/js/JQueryLib.js'></script>
  	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


	<!--  CSS -->
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/main.css">

	<!-- FONTS -->
	<link href='https://fonts.googleapis.com/css?family=Lobster|Abril+Fatface' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Cinzel:400,700' rel='stylesheet' type='text/css'>

	<!-- BOOTSTRAP.js -->
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

	<!-- JQUERY.js -->
	<script src="/assets/js/JQueryLib.js"></script>


	<!-- JQUERY UI.js -->
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

	<script>
 	$(document).ready(function(){

 		$(function() {
    		$( "#tabs" ).tabs();
  		});

  		var activeLi = $(".tabs > ul > li.active");
			var activeIndex = $(".tabs > ul > li").index(activeLi);
			$(".tab-content:first").show();
			var arrowPos = activeLi.width() * (activeIndex + 0.5) - 12;
			$("#tab-container .arrow").css("left", arrowPos);

			$(window).resize(function() {
			  var activeLi = $(".tabs > ul > li.active");
			  var activeIndex = $(".tabs > ul > li").index(activeLi);
			  var arrowPos = activeLi.width() * (activeIndex + 0.5) - 12;
			  $("#tab-container .arrow").css("left", arrowPos);
			});

			$(".tabs > ul > li ").click(function() {
			  var listItem = $(this).closest("li");
			  if ($(listItem).hasClass("active")) {
			    return;
			  }
			  $(".tab-content").hide();
			  var activeTab = $(listItem).attr("rel");
			  var tabList = $(".tabs > ul > li");
			  var arrow = $("#tab-container .arrow");
			  var index = tabList.index($(listItem));
			  var width = $(listItem).width();
			  var leftValue = width * (index + 0.5) - 12;
			  arrow.css("left", leftValue);
			  $("#" + activeTab).fadeIn();
			  
			  tabList.removeClass("active");
			  $(listItem).addClass("active");
			});
 	})

	    // AJAX Loading Spinner
	    $(document).ready(function(){
	    var $loading = $('.loadingDiv').hide();
	    $(document)
	  		.ajaxStart(function () {
	        	$loading.show();
	      	})
	      	.ajaxStop(function () {
	        	$loading.hide();
	      	});
	    })	

  	</script>
</head>
<body>

	<?php $this->load->view('navbar'); ?>

	<section id='photos'>
		<div style=''>
				<div class="tabs">
				  <ul>
				    <li class="active" rel="tab1">Interior</li>
				    <li rel="tab2">Exterior</li>
				    <li rel="tab3">Views</li>
				    <li rel="tab4">Tenants</li>
				  </ul>
				  
				  <div id="tab-container">
				    <div class="arrow"></div>
				    <!-- Interior Photos -->
				    <div id="tab1" class="tab-content">
				    	<figure>
				    		<img src="/assets/images/lobby_full.svg">
				    	</figure>
				    	<figure>
				    		<img src="/assets/images/lobby1.svg">
				    	</figure>
				    	<figure>
				    		<img src="/assets/images/lobby2.svg">
				    	</figure>
			    		<figure>
			    			<img src="/assets/images/pnwt_fireplace.svg">
			    		</figure>
			    		<figure>
			    			<img src="/assets/images/pnwt_lobby.svg">
			    		</figure>
			    		<figure>
			    			<img src="/assets/images/pnwt_conference.svg">
			    		</figure>
				    	<figure>
			    			<img src="/assets/images/entrence.svg">
			    		</figure>
			    		<figure>
			    			<img src="/assets/images/interior_scott_stewart.svg">
			    		</figure>
				    </div>
				    <!-- Exterior Photos -->
				    <div id="tab2" class="tab-content">
			    		<figure>
			    			<img src="/assets/images/cavalon_front_sign2.svg">
			    		</figure>
			    		<figure>
			    			<img src="/assets/images/entrence2.svg">
			    		</figure>
			    		<figure>
			    			<img src="/assets/images/frontview.svg">
			    		</figure>
			    		<figure>
			    			<img src="/assets/images/sideview.svg">
			    		</figure>
			    		<figure>
			    			<img src="/assets/images/sideview2.svg">
			    		</figure>
			    		<figure>
			    			<img src="/assets/images/patio_full.svg">
			    		</figure>
			    		<figure>
			    			<img src="/assets/images/patio_stairs.svg">
			    		</figure>
			    		<figure>
			    			<img src="/assets/images/garage1.svg">
			    		</figure>
			    		<figure>
			    			<img src="/assets/images/garage2.svg">
			    		</figure>
			    		<figure>
			    			<img src="/assets/images/patio.svg">
			    		</figure>
			    		<figure>
			    			<img src="/assets/images/cavalon_front_door.svg">
			    		</figure>
			    		<figure>
			    			<img src="/assets/images/patio_deck.svg">
			    		</figure>
			    		<figure>
			    			<img src="/assets/images/balcony.svg">
			    		</figure>
			    		<figure>
			    			<img src="/assets/images/cavalon_night.svg">
			    		</figure>
				    </div>
				    <!-- Views Photos -->
				    <div id="tab3" class="tab-content">
				    	<figure>
			    			<img src="/assets/images/view_olympics.svg">
			    		</figure>
			    		<figure>
			    			<img src="/assets/images/view_olympics2.svg">
			    		</figure>
			    		<figure>
			    			<img src="/assets/images/view_silverdale.svg">
			    		</figure>
			    		<figure>
			    			<img src="/assets/images/view_waterfront.svg">
			    		</figure>
			    		<figure>
			    			<img src="/assets/images/view_silverdale2.svg">
			    		</figure>
				    </div>
				    <!-- Tenants Photos -->
				    <div id="tab4" class="tab-content">
				      	<figure>
				      		<img src="/assets/images/tenant_pnwt.svg">
				      	</figure>
				  </div>
				</div>
			</div>
	</section>

	<section id='photo_form'>
		<div class='container'>
			<h2 class='glyphicon_header'><i class='glyphicon glyphicon-camera'></i><h2><h2>Did you capture a great view from Cavalon?</h2><h2 class='glyphicon_header'><i class='glyphicon glyphicon-camera'></i></h2>
			<h3>Send in your photo and we'll place it in our photo album!</h3>

			 <div class="row">
                <div class="col-md-8 col-md-offset-2 margin30 sky-form-columns">
                    <div class='message'></div>
                    <form class="sky-form" action="/contact-message" method="post" id="sky-form">
                        <div class='loadingDiv' style='width: 50px; height: 50px; margin: 0 auto;  z-index: 9999;'>
                            <i style='font-size: 50px; color: lightblue; display: inline-block;' class="fa fa-cog fa-spin"><i style='font-size: 50px; color: lightblue; display: inline-block;' class="fa fa-cog fa-spin"></i></i>
                        </div>
                        <fieldset>                  
                            <div class="row">
                                <section class="col col-6">
                                    <label class="label">Name</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="text" name="name" id="name" required>
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="label">E-mail</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-envelope-o"></i>
                                        <input type="email" name="email" id="email" required>
                                    </label>
                                </section>
                            </div>

                             <section>
                                <label class="label">Caption</label>
                                <label class="input">
                                    <i class="icon-append fa fa-comment"></i>
                                    <input type="text" name="caption" id="caption" required>
                                </label>
                            </section>
                            
                            <section>
                                <label class="label">Photo Upload</label>
                                <label class="input">
                                    <i class="icon-append fa fa-file"></i>
                                    <input type="file" name="photo" id="photo" required>
                                </label>
                            </section>
                            
                             <section>
                                <label class="label">Enter characters below:</label>
                                
                                <?php echo $image; ?>
                                <label class="input input-captcha" id='captcha'>
                                	<img src="/assets/icons/robot30.svg" class='icon-append' style='width:33px; height:29px;'>
                                	<!-- <i class='icon-append fa fa-android'></i> -->
                                    <input type="text" maxlength="8" name="captcha" id="captcha" placeholder="Prove you're human" required>
                                </label>
                            </section>
                            
                        </fieldset>
                        <div class='loadingDiv' style='width: 50px; height: 50px; margin: 0 auto;  z-index: 9999;'>
                            <i style='font-size: 50px; color: lightblue; display: inline-block;' class="fa fa-cog fa-spin"><i style='font-size: 50px; color: lightblue; display: inline-block;' class="fa fa-cog fa-spin"></i></i>
                        </div>
                        
                        <button type="submit" class="btn border-white btn-lg">Let's Chat</button>
                        
                        
                        <div class="message">
                            <i class="fa fa-check"></i>
                            <p>Your message was successfully sent!</p>
                        </div>
                    </form> 
                   
                </div>
                <!-- <div class="col-md-4">
                    <h3 class="heading">Contact info</h3>
                    <ul class="list-unstyled contact contact-info">
                        <li>
                            <p><strong><i class="fa fa-map-marker"></i> Address:</strong> 220 2nd Ave S</p>
                            <p id='address_alignment'>Seattle, WA 98105</p>
                        </li> 
                        <li><p><strong><i class="fa fa-envelope"></i> Mail Us:</strong> <a href="mailto:info@uxacademy.io">info@uxacademy.io</a></p></li>
                    </ul>
                    <div class="divide40"></div>
                    -->
            </div>
		</div>
	</section>
	
	<?php $this->load->view('footer'); ?>

</body>
</html>