<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Navigation extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){
		if(!isset($_SESSION)){
		session_start();
		}
		$this->load->view('landing_page');
	}

	public function available_space(){
		/* Setup vals to pass into the create_captcha function */
		$vals = array(
			'img_path' => './captcha/',
			'img_url' => '/captcha/',
			'img_width' => '150',
			'img_height' => 50,
			'expiration' => 7200,
		);
      	
      	/* Generate the captcha */
      	$captcha = create_captcha($vals);
      
      	/* Store the captcha value (or 'word') in a session to retrieve later */
      	$this->session->set_userdata('captchaWord', $captcha['word']);
      
      	/* Load the captcha view containing the form (located under the 'views' folder) */
      	$this->load->view('available_space', $captcha);
	}

	public function floor_plans(){
		$this->load->view('floor_plans');
	}

	public function maps(){
		$this->load->view('maps');
	}

	public function photos(){
		/* Setup vals to pass into the create_captcha function */
		$vals = array(
			'img_path' => './captcha/',
			'img_url' => '/captcha/',
			'img_width' => '150',
			'img_height' => 50,
			'expiration' => 7200,
		);
      	
      	/* Generate the captcha */
      	$captcha = create_captcha($vals);
      
      	/* Store the captcha value (or 'word') in a session to retrieve later */
      	$this->session->set_userdata('captchaWord', $captcha['word']);
      
      	/* Load the captcha view containing the form (located under the 'views' folder) */
      	$this->load->view('photos', $captcha);
	}

	public function contact_us(){
		/* Setup vals to pass into the create_captcha function */
		$vals = array(
			'img_path' => './captcha/',
			'img_url' => './captcha/',
			'img_width' => '150',
			'img_height' => 50,
			'expiration' => 7200,
		);
      	
      	/* Generate the captcha */
      	$captcha = create_captcha($vals);
      
      	/* Store the captcha value (or 'word') in a session to retrieve later */
      	$this->session->set_userdata('captchaWord', $captcha['word']);
      
      	/* Load the captcha view containing the form (located under the 'views' folder) */
      	$this->load->view('contact_us', $captcha);
	}
}
