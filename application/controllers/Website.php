<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Website extends CI_Controller {

	public function __construct()
	  {
	    parent::__construct();
	    /* Load the libraries and helpers */
	    $this->load->library('form_validation');
	    $this->load->library('session');
	    $this->load->helper(array('form', 'url', 'captcha'));
	  }

	public function contact_us(){

		if( strtoupper($_POST['captcha']) == strtoupper($_SESSION['captchaWord']) ){
			var_dump($this->input->post());
			die('working cpatcha');
		}else{
			echo json_encode('Error with security code.');
		}
	}

	public function inquire_available_space(){
		$accepted = true;

		if($accepted){
			echo json_encode("Thank you for inquiring in Cavalon's available space."."<br><br>"." We will be in contact with you soon!");
		}
		else{
			echo json_encode("We're sorry there was an error talking with the server."."<br><br>"."Please try again in a few minutes.");
		}
	}
}
