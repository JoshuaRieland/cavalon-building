<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tenant extends CI_Controller {

	public function signin(){
		$loginAttempt = $this->input->post(); //username & password
		$loginCredentials = $this->Cavalon->getLoginCredentials($loginAttempt['username']);
		// die(var_dump($loginCredentials));
		if($loginCredentials == null || ($loginCredentials['salt'] == 'saltNeeded' && $loginAttempt['password'] != $loginCredentials['password'])){
			$this->load->view('unknown_user');
		}
		else if($loginCredentials['salt'] == 'saltNeeded'){
			$_SESSION['userID'] = $loginCredentials['id'];
			$_SESSION['username'] = $loginCredentials['username'];
			$this->load->view('register_user', array('userID' => $loginCredentials['id']));
		}else if($loginCredentials['salt'] != 'saltNeeded'){
			$passwordCheck = crypt($loginAttempt['password'],$loginCredentials['salt']);
			if($passwordCheck != $loginCredentials['password']){
				$this->load->view('unknown_user');
			}else{
				$_SESSION["status"] = 'Active';
				$_SESSION['userID'] = $loginCredentials['id'];
				$_SESSION["tenantInfo"] = $this->Cavalon->getTenantInfo($_SESSION['userID']);
				$allNewMessages = $this->Cavalon->getNewMessageCount($_SESSION['userID']);
				$_SESSION["newMessagesCount"] = count($allNewMessages);
				$this->load->view('tenant_dashboard');
			}
		}
	}
	public function signout(){
		$_SESSION['status'] = 'Inactive';
		$_SESSION['username'] = '';
		$_SESSION['tenantInfo'] = '';
		$_SESSION['newMessagesCount'] = '';
		$this->load->view('landing_page');
	}

	public function settings(){
		$this->load->view('account_settings');
	}

	public function dashboard(){
		$allNewMessages = $this->Cavalon->getNewMessageCount($_SESSION['userID']);
		$_SESSION["newMessagesCount"] = count($allNewMessages);
		$this->load->view('tenant_dashboard');
	}


	public function update_password(){
		$passwordInfo = $this->input->post(); // new password & userID
		$salt = random_string();
		$passwordInfo['password'] = crypt($passwordInfo['new_password'],$salt);
		$passwordInfo['salt'] = $salt;
		$updated = $this->Cavalon->updatePassword($passwordInfo);
		if($updated){
			$this->load->view('register_user_step2');
		}else{
			echo ('uh oh');
		}
	}

	public function update_tenant(){
		$tenantInfo = $this->input->post(); // first name, last name, email & userID
		$updated = $this->Cavalon->updateTenant($tenantInfo);
		if($updated){
			$_SESSION['tenantInfo'] = $tenantInfo;
			$this->load->view('tenant_dashboard');
		}
	}

	public function messages(){
		$allMessages = $this->Cavalon->getAllMessages($_SESSION['userID']);
		$this->load->view('messages', array('allMessages' => $allMessages));
	}
	
}
